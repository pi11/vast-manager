#!/bin/bash

dir=$(dirname $(which $0));
cd $dir
source ../../ve/bin/activate
source ../../../ve/bin/activate
trap dobreak INT

function dobreak(){
	stoped="1"
	echo 'Server stoped by user request...'
	
}
stoped="0"

while [ $stoped == "0" ]
do
	python main.py
	if [ $stoped == "0" ]; then
		echo "Server terminated, restarting in a few moments..."
		sleep 0.5
	fi	
done  
