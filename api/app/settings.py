# -*- coding: utf-8 -*-
"""Configuration variables"""

import os.path

HOST = '0.0.0.0'
PORT = 8080

DB_ROUTER = {
    "default": {
        "DBNAME": "",
        "DBUSER": "",
        "DBHOST": "localhost",
        "DBPASSWORD": "",
        "DBPORT": "5432",
    },
}

ABS_ROOT =  os.path.abspath(os.path.dirname(__file__))
UPLOAD_DIR = os.path.join(ABS_ROOT, '../media')
TEMPLATES = os.path.join(ABS_ROOT, '../templates')
# print(TEMPLATES)
STATIC_URL = "http://localhost/vast-manager/static/"
MAX_VIEWS_BEFORE_UPDATE = 100  # how many views should be cached before update database
WORKERS = 4
CACHE_TIMEOUT = 60 * 30 # 30 minutes
CONNECTION_POOL_SIZE = 20
ACCESS_LOGS = True
AUTH_COOKIE = "vast-manager"
COOKIE_NAME = "adv1"
PROXIES_COUNT = 1

from local_settings import *
