# -*- coding: utf-8 -*-

import sys
import hashlib
def add_user(username, password):
    hash_pass = hashlib.sha224(password.encode('utf8')).hexdigest()
    print("""
    insert into account(username, password) values ('%s', '%s');
    """ % (username, hash_pass))

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: add_user username password")
    else:
        add_user(sys.argv[1], sys.argv[2])
