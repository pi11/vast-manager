#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""vast manager service"""

import traceback
from datetime import datetime
import random

import pylibmc
from user_agents import parse

from sanic import Sanic
from sanic.exceptions import NotFound
from sanic.response import text, json
from jinja2 import FileSystemLoader, Environment
from helpers import (get_connections, get_connection, pickle_record,
                     get_ip_from_request)

app = Sanic('vast')
app.config.from_pyfile('settings.py')


@app.listener('before_server_start')
async def init_app(app, loop):
    """configure some settings for app"""
    app.config.m_client = pylibmc.Client(['127.0.0.1'], binary=True,
                                         behaviors={"tcp_nodelay": True,})
    app.config.j_env = Environment(loader=FileSystemLoader(app.config.TEMPLATES))
    app.config.connections = await get_connections(app.config.DB_ROUTER, loop)


def render(request, template_name, params={}, status=200, content_type="text/html"):
    """Render template"""
    params.update(app.config.TEMPLATE_LOCALS) # fix me
    return text(app.config.j_env.get_template(template_name).render(params),
                status=status, content_type=content_type)


@app.get('/api/get/<vast_id:int>', strict_slashes=False)
async def vast(request, vast_id):
    """Vast xml
    cookie with client id:
    vast_id-impid1|impid2 ..."""

    def select_ad(ads, impressions, country_code, all_showed=False):
        """in_c - include countries, ex_c - exclude countries
        both params could be None, all_showed - """
        AD = False
        for ad in ads:
            ad_ = pickle_record(ad)
            #print("Processing: %s" % ad_)
            ex_c = ad_['exclude_countries']  #.strip()
            in_c = ad_['only_countries']  #.strip()
            
            if ad_['id'] in impressions and not all_showed:
                print("Already shown, skip AD[%s], check next one" % ad_['id'])
                continue  # next AD
            if country_code and ex_c:
                if country_code.lower() in ex_c.lower():
                    print("Exclude country: %s from %s" % (country_code, ex_c))
                    continue
            if country_code and in_c:
                if country_code.lower() in in_c.lower():
                    AD = ad_
                    break
                else:
                    continue
            if in_c and not country_code:
                continue
            AD = ad_
            break
        
        #print("Selected AD: %s" % AD['id'])
        return AD

    ip = get_ip_from_request(request)
    #print(ip)
    ua = request.headers.get('user-agent')
    user_agent = parse(ua)
    if user_agent.is_bot:
        return text('(_!_)')
    #client_type = []
    ads_query = "select * from manager_ad where (vast_id=$1 and is_active=True) and (1=2 "

    if user_agent.os.family == 'iOS':
        ads_query += "or is_ios=True "
    if user_agent.os.family == 'Android':
        ads_query += "or is_android=True "
    if user_agent.is_mobile:
        ads_query += "or is_mobile=True "
    if user_agent.is_pc:
        ads_query += "or is_pc=True "
    ads_query += ') order by "order"'
    #print(ads_query)

    pool = get_connection(app)
    query = "select * from manager_vast where id=$1"

    async with pool.acquire() as conn:
        vast = pickle_record(await conn.fetchrow(query, int(vast_id)))

    added = datetime.now()
    client_id = None
    impressions = False
    cookie_name = '%s-%s' % (app.config.COOKIE_NAME, vast_id)
    
    async with pool.acquire() as conn:
        ads = request.cookies.get(cookie_name)
        if ads:
            try:
                client_id = int(ads.split('-')[0])
                impressions = [int(x) for x in ads.split('-')[1].split('|')]
                if len(impressions) >= vast['views_per_day']:
                    print("Total %s impressions. Skip vast. Client ip: %s" %
                          (len(impressions), ip))
                    return render(request, 'empty.xml', {}, content_type='text/xml')
                #print(impressions)

                query = "select * from manager_client where id=$1"
                client = pickle_record(await conn.fetchrow(query, client_id))
            except:
                print(traceback.format_exc())
                client_id = None

        if not ads and not client_id:
            # new client
            # we need to set client_id cookie for it
            query = "select * from manager_client where ip=$1 and ua=$2"
            client = pickle_record(await conn.fetchrow(query, ip, ua))
            if not client:
                ins_q = "insert into manager_client(ip, ua, added) values ($1, $2, $3)"
                await conn.execute(ins_q, ip, ua, added)
                client = pickle_record(await conn.fetchrow(query, ip, ua))
            client_id = client['id']
        if client_id:  # save client request to vast
            ins_q = "insert into manager_adrequest(vast_id, client_id, added) values($1, $2, NOW())"
            await conn.execute(ins_q, vast_id, client_id)

        user_country_q = ("select code_name from countries_codes where code = "
                          "(select country_code from countries_ips where address >>= $1)")
        client_country = pickle_record(await conn.fetchrow(user_country_q, ip))
        if client_country:
            country_code = client_country['code_name']
        else:
            country_code = None

        if not impressions:
            impressions = []

        #print(ads_query)
        ads = await conn.fetch(ads_query, vast_id)
        AD = select_ad(ads, impressions, country_code)
        #print("Selected AD: ", AD['id'])
        if not AD:
            ads_query = ads_query.replace('"order"', 'RANDOM()')
            ads = await conn.fetch(ads_query, vast_id)
            AD = select_ad(ads, impressions, country_code, all_showed=True)
            
        if AD:
            if not impressions:
                impressions_list = AD['id']

            else:
                impressions.append(AD['id'])
                impressions_list = "|".join([str(i) for i in impressions])
            query = 'select * from manager_video where id=$1'
            video = pickle_record(await conn.fetchrow(query, AD['media_id']))
        else:
            impressions_list = [0]
            video = False

    imp_hash = random.getrandbits(20)
    if not AD:
        return render(request, 'empty.xml', {}, content_type='text/xml')

    r = render(request, 'vast.xml', {"imp_hash":imp_hash, "v":AD, 'video':video,
                                     "MEDIA_URL": app.config.MEDIA_URL},
               content_type='text/xml')
    r.cookies[cookie_name] = "%s-%s" % (client_id, impressions_list)
    r.cookies[cookie_name]['max-age'] = 60 * 60 * 20 # cookie for 20 hours
    r.cookies[cookie_name]['httponly'] = False #60 * 60 * 20 # cookie for 20 hours
    r.cookies[cookie_name]['domain'] = 'v.scurra.space' #60 * 60 * 20 # cookie for 20 hours
    #print("Client id:", client_id)
    return r


@app.get('/api/imp/<action:int>/<vast_id:int>/<ad_id:int>/<imp_hash:int>',
         strict_slashes=False)
async def imp(request, action, vast_id, ad_id, imp_hash):
    """ cookie with client id:
    vast_id-impid1|impid2 ..."""
    ip = get_ip_from_request(request)
    ua = request.headers.get('user-agent')
    pool = get_connection(app)
    added = datetime.now()
    cookie_name = '%s-%s' % (app.config.COOKIE_NAME, vast_id)
    ads = request.cookies.get(cookie_name)
    async with pool.acquire() as conn:
        try:
            client_id = int(ads.split('-')[0])
        except:
            print("Client cookie does not exists or broken")
            #print(traceback.format_exc())
            query = "select * from manager_client where ip=$1 and ua=$2"
            client = pickle_record(await conn.fetchrow(query, ip, ua))
            if not client:
                ins_q = "insert into manager_client(ip, ua, added) values ($1, $2, $3)"
                await conn.execute(ins_q, ip, ua, added)
                client = pickle_record(await conn.fetchrow(query, ip, ua))
            client_id = client['id']
        query = "select * from manager_impression where client_id=$1 and imp_hash=$2 and ad_id=$3"
        test_imp = pickle_record(await conn.fetchrow(query, client_id, imp_hash,
                                                     ad_id))
        if not test_imp:
            ins_q = ("insert into manager_impression(action, client_id, ad_id,"
                     " imp_hash, added) values ($1, $2, $3, $4, $5)")
            try:
                await conn.execute(ins_q, action, client_id, ad_id, imp_hash, added)
            except:
                print("=" * 22)
                print ("Error saving impressions in log")
                print("Cookiename: %s, ads: %s" % (cookie_name, ads))
                print("IP: [%s], UA: [%s]" % (ip, ua))
    r = text(".")
    return r


@app.get('/api/eximp/<imp>', strict_slashes=False)
async def ex_imp(request, imp):
    pool = get_connection(app)
    added = datetime.now()
    async with pool.acquire() as conn:
        ins_q = "insert into manager_eximp(imp, added) values ($1, $2)"
        await conn.execute(ins_q, imp, added)
    r = text(".")
    return r


@app.post('/tmp-stats', strict_slashes=False)
async def tmp_stats(request):
    print("VAST:%s" % request.body.decode('utf-8'))
    return json({'result': 0}, status=200)


@app.exception(NotFound)
def ignore_404s(request, exception):
    """Custom 404 pages"""
    #ip = (request.remote_addr or request.ip)#request.ip
    #print("404")
    #print(request.headers)
    return text("Страница не найдена", status=404)


if __name__ == '__main__':
    app.run(host=app.config.HOST, port=app.config.PORT,
            workers=app.config.WORKERS, debug=False,
            access_log=app.config.ACCESS_LOGS)
