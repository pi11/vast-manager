# -*- coding: utf-8 -*-
"""Some helper functions"""

import traceback
import asyncpg
from settings import CONNECTION_POOL_SIZE
#  from contextlib import contextmanager


def get_ip_from_request(request):
    ip = (request.remote_addr or request.ip)
    if ip == '127.0.0.1':
        ip = request.headers.get('X-Forwarded-For')
    if ip == '127.0.0.1':
        ip = request.headers.get('X-Real-IP')
    if not ip:
        ip = "127.0.0.1"
    if ip == '127.0.0.1':
        #print("=" * 30)
        #print("Attention: No IP found!")
        #print(request.headers)
        #print("=" * 30)
        pass
    if len(str(ip)) > len('255.255.255.255'):
        ip = "127.0.0.1"
    return ip

    
async def get_connections(db_router, loop):
    """Iterate over databases and return dictionary with db connections"""
    connection = {}
    for k, val in db_router.items():
        pool = await asyncpg.create_pool(
            database=db_router[k]['DBNAME'], user=db_router[k]['DBUSER'],
            host=db_router[k]['DBHOST'], password=db_router[k]['DBPASSWORD'],
            port=db_router[k]['DBPORT'], loop=loop,
            min_size=CONNECTION_POOL_SIZE-20, max_size=CONNECTION_POOL_SIZE+1)
        connection[k] = pool
    return connection


def get_connection(app):
    conn = app.config.connections['default']
    return conn


def pickle_record(rec):
    if not rec:
        return rec
    pickled = {}
    for k,v in rec.items():
        pickled[k] = v
    return pickled


async def fetch_query(conn, query, *args):
    try:
        result = await conn.fetch(query, *args)
    except (asyncpg.InterfaceError, asyncpg.exceptions.DataError,
                TypeError):
        print(traceback.format_exc())            
    return result
