from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.db import models
from django.conf import settings


ACTION_CHOICES = [
    (0, 'View'),
    (1, 'Click'),
]


class CountryCode(models.Model):
    name = models.CharField(max_length=250)
    code_name = models.CharField(max_length=4)
    code = models.IntegerField(unique=True, editable=False)

    class Meta:
        db_table = "countries_codes"

    def __str__(self):
        return self.name


class Video(models.Model):
    name = models.CharField(max_length=250, unique=True)
    video = models.FileField(upload_to="", verbose_name="Видео")

    def __str__(self):
        return self.name


class Impression(models.Model):
    ad = models.ForeignKey('AD', on_delete=models.CASCADE)
    client = models.ForeignKey('Client', on_delete=models.CASCADE)
    added = models.DateTimeField(auto_now_add=True)
    action = models.PositiveSmallIntegerField(choices=ACTION_CHOICES)
    imp_hash = models.IntegerField()
    
    def __str__(self):
        return str(self.ad)


class AD(models.Model):
    vast = models.ForeignKey('Vast', on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True)
    media = models.ForeignKey(Video, on_delete=models.SET_NULL, null=True)
    ad_url = models.CharField(max_length=250, verbose_name="Рекламная ссылка")
    order = models.IntegerField(default=0, verbose_name="Порядок")
    is_active = models.BooleanField(default=True, verbose_name="Объявление активно")
    is_ios = models.BooleanField(default=True, verbose_name="Показывать на iOS")
    is_android = models.BooleanField(default=True, verbose_name="Показывать на Android")
    is_mobile = models.BooleanField(default=True, verbose_name="Показывать на Мобилах")
    is_pc = models.BooleanField(default=True, verbose_name="Показывать на ПК")
    only_countries = models.CharField(
        max_length=250, null=True, blank=True,
        verbose_name="Список кодов стран для показа через запятую")
    exclude_countries = models.CharField(
        max_length=250, null=True, blank=True,
        verbose_name="Список кодов стран где НЕ показывать объявление")

    def __str__(self):
        return str(self.vast)

    def last_24hours_impressions(self):
        n = datetime.now()
        day = timedelta(hours=24)
        return Impression.objects.filter(ad=self, added__gte=n-day).count()
    last_24hours_impressions.short_description = "Показы за последние 24 часа"


class Vast(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, unique=True)
    views_per_day = models.IntegerField(
        default=4, verbose_name="Количество показов в день")

    def __str__(self):
        return self.name

    def code(self):
        return "%sapi/get/%s" % (settings.VAST_BASE_URL, self.pk)

    def ADs_count(self):
        return AD.objects.filter(vast=self).count()


class Client(models.Model):
    """tmp model for client
    should be emptied every 48 hours? lets test it"""
    ip = models.CharField(max_length=50)
    ua = models.CharField(max_length=500)
    added = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.ip


class AdRequest(models.Model):
    vast = models.ForeignKey(Vast, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, null=True, on_delete=models.SET_NULL)
    added = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return str(self.vast)


class ExImp(models.Model):
    """Impressions from external vasts for test"""

    imp = models.TextField()
    added = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return str(self.pk)
