from django.contrib import admin
from manager.models import AD, Vast, Impression, Video, CountryCode


class ADInline(admin.StackedInline):
    model = AD


class VastAdmin(admin.ModelAdmin):
    list_display = ("name", "user", "code", "ADs_count")
    inlines = [
        ADInline,
    ]


admin.site.register(Vast, VastAdmin)


class ADAdmin(admin.ModelAdmin):
    list_display = ("vast", "ad_url", "description", "order",
                    "last_24hours_impressions")


admin.site.register(AD, ADAdmin)


class ImpressionAdmin(admin.ModelAdmin):
    list_display = ("client", "added", "action")


admin.site.register(Impression, ImpressionAdmin)


class VideoAdmin(admin.ModelAdmin):
    list_display = ("name", )


admin.site.register(Video, VideoAdmin)


class CountryCodeAdmin(admin.ModelAdmin):
    list_display = ("name", "code_name", "code", )
    search_fields = ("name", "code_name", "code")


admin.site.register(CountryCode, CountryCodeAdmin)
