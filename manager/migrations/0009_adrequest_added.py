# Generated by Django 2.2.5 on 2019-11-06 01:01

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0008_auto_20191106_0051'),
    ]

    operations = [
        migrations.AddField(
            model_name='adrequest',
            name='added',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
