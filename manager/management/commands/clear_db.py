#-*- coding: utf-8 -*-
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand, CommandError
from django.db import connection

from manager.models import AdRequest, Impression, Client, ExImp


class Command(BaseCommand):
    help = """This command clear database"""

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS(
            "Clearing database"))
        d = datetime.now() - timedelta(hours=48)
        AdRequest.objects.filter(added__lt=d).delete()
        Impression.objects.filter(added__lt=d).delete()
        print("Deleting clients")
        deleted = 10
        while deleted > 0:
            ids = [cl.pk for cl in Client.objects.filter(added__lt=d)[:1000]]
            a = Client.objects.filter(id__in=ids).delete()
            deleted = a[0]
            print("Deleted {} from manager_client".format(deleted), flush=True)
        deleted = 10
        while deleted > 0:
            
            ids = [cl.pk for cl in ExImp.objects.filter(added__lt=d)[:1000]]
            a = ExImp.objects.filter(id__in=ids).delete()
            deleted = a[0]
            print("Deleted {} from manager_eximp".format(deleted), flush=True)
